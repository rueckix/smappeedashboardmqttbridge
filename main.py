import json
import sys
import time
import os
import os.path
from typing import List

from datetime import date, datetime, timedelta

from typing import Dict, Tuple

import requests
import paho.mqtt.client as mqtt 


class State:
    last_message = 0
    last_power = 0
    last_gas = 0
    last_water = 0
    seen_events = []
    
    def __init__(self, last_message: int, last_power: int, last_gas: int, last_water: int,  seen_events: List[int]):
        self.last_message = last_message
        self.last_power = last_power
        self.last_gas = last_gas
        self.last_water = last_water
        self.seen_events = seen_events

state = State(0,0,0,0,[])
# state.json
statefile = "state.json"

# /run/secrets is created by the docker swarm environment


secretsPath = "/run/secrets"
localSecretsPath = "secrets"

class Config:
    api_login = "https://dashboard.smappee.net/dashapi/login"
    api_messages = "https://dashboard.smappee.net/api/v9/messages"
    _api_events = "https://dashboard.smappee.net/api/v9/servicelocation/{0}/recentevents?pageSize=50"
    api_power = "https://dashboard.smappee.net/api/v10/servicelocation/{location}/consumption?from={f}&to={t}&reportType=HOUR"
    api_consumption = "https://dashboard.smappee.net/api/v9/servicelocation/{location}/usage?aggregationType=MINUTES_5&intervalLength=DAY&range={f},{t}&types={type}"
    api_cred = ""
    api_serviceLocation = ""
    api_token = ""
    mqtt_host = ""
    mqtt_port = 0
    mqtt_user = ""
    mqtt_pass = ""
    debug = 0
    eventHistory = 50
    
    sleep = 1
    
    def getEventUrl(self)->str:
        return self._api_events.format(self.api_serviceLocation)

    def getPowerUrl(self) -> str:
        now = datetime.now()
        onehour = timedelta(hours=1)
        now = now - onehour
        # set to end of previous hour
        now = now.replace(minute=59)
        now = now.replace(second=59)
        now = now.replace(microsecond=999000)
        
        oneday = timedelta(days=1)
        onemilli = timedelta(microseconds=1000)

        yesterday = now - oneday + onemilli

        today_ts = int(now.timestamp() * 1000)
        yesterday_ts = int(yesterday.timestamp() *1000)

        return self.api_power.format(location=self.api_serviceLocation,f=yesterday_ts, t=today_ts)

    def _getConsumptionTimeInterval(self) -> Tuple:
        # set to end of today
        now = datetime.today()
        now = now.replace(hour = 23)
        now = now.replace(minute = 55)
        now = now.replace(second = 0)
        now = now.replace(microsecond = 0)

        onesecond = timedelta(seconds=1)
        oneday = timedelta(days=1)

        yesterday = now - oneday
        today = now + onesecond

        today_ts = int(today.timestamp() * 1000)
        yesterday_ts = int(yesterday.timestamp() *1000)
        return (yesterday_ts, today_ts)
    
    def getGasUrl(self) -> str:
        (yesterday_ts,today_ts) = self._getConsumptionTimeInterval()
        return self.api_consumption.format(location=self.api_serviceLocation,f=yesterday_ts, t=today_ts, type="GAS")
    
    def getWaterUrl(self) -> str:
        (yesterday_ts,today_ts) = self._getConsumptionTimeInterval()
        return self.api_consumption.format(location=self.api_serviceLocation,f=yesterday_ts, t=today_ts, type="WATER")

    def getMqttEventPrefix(self)->str:
        return "SmappeeWebAPI/{s}/".format(s=self.api_serviceLocation)
    
    def getMqttAlertPrefix(self)->str:
        return "SmappeeWebAPI/Alert/"

    def getMqttConsumptionPrefix(self)->str:
        return "SmappeeWebAPI/Consumption/"
        


config = Config()
mqttClient = mqtt.Client("SmappeeWebApi")

def logError(txt):
    print(txt, file=sys.stderr)

def debug(txt):
    if (config.debug == 1):
        print(str(datetime.now())+  "\t" + txt, file=sys.stdout)
        
def readState():
    # read configuration from file /config.json
    global state

    try:
        with open(statefile, 'r') as infile:
            data = infile.readline()
            #print(data)
            state = State(**json.loads(data))

            print("Loading last state: " + data)
            #print (state.__dict__)
            
            
    except Exception as e:
        logError("State file does not exist or could not be parsed.")
        state = State(0,0,0,0,[])

def writeState():
    # write configuration to file    
    with open(statefile, 'w') as outfile:
        json.dump(state.__dict__, outfile)
    

def readEnv():
    # POLL_SLEEP
    # MQTT_HOST
    # MQTT_PORT
    
    global config

    config.mqtt_host = os.environ.get("MQTT_HOST", "srv-mqtt.domain")
    config.mqtt_port = int(os.environ.get("MQTT_PORT", "1883"))
    config.sleep = int(os.environ.get("POLL_SLEEP", "5"))
    config.debug = int(os.environ.get("DEBUG", "1"))

    config.eventHistory = int(os.environ.get("EVENT_HISTORY_SIZE", "50"))

    #print(config.__dict__)


def get_secret(name):
    fname = ""

    # test if the swarm secret directory exists. Otherwise fall back to local "secret" directory
    if os.path.isdir(secretsPath):
        fname = secretsPath
    else:
        fname = localSecretsPath

    with open(fname + "/" + name, 'r') as sfile:
        return sfile.read().strip()

def readSecrets():
    
    # MQTT_USER
    # MQTT_PASS
    # SMAPPEE_USER
    # SMAPPEE_PASS

    #config.api_cred = {"userName":"svc-smappee","password":"44e4c3def8b76fe"}
    #config.mqtt_user = "smappee"
    #config.mqtt_pass = "lf0lbx6udbSZ8SNVIomd"

    global config

    config.mqtt_user = get_secret("MQTT_USER")
    config.mqtt_pass = get_secret("MQTT_PASS")
    config.api_cred = {"userName":"{0}".format(get_secret("SMAPPEE_USER")),"password":"{0}".format(get_secret("SMAPPEE_PASS"))}

    #print(config.__dict__)



def apiConnect():
    # throw an error if not successful

    # test if we already have a token
    if (len(config.api_token) > 0):
        return


    login_hdr = {"Content-Type": "application/json"}
    resp_login = requests.post(url=config.api_login, data=json.dumps(config.api_cred), headers=login_hdr)
    if (resp_login.status_code != 200):
        raise ConnectionError("Login failed. Code: " + str(resp_login.status_code))

    config.api_serviceLocation = resp_login.json()["activeServiceLocation"]["id"]
    config.api_token = {"Token": resp_login.json()["token"]}
    debug("apiConnect: login")



def apiMessages() -> Dict:
    # fetch message later than last timestamp
    # return a dictionary

    apiConnect()

    resp_messages = requests.get(url=config.api_messages, headers=config.api_token)
    if (resp_messages.status_code != 200):
        raise ConnectionError("Could not fetch messages. Code: " + str(resp_messages.status_code))
    
    
    msg_alert = [x for x in resp_messages.json() if x["timestamp"] > state.last_message and x["type"] == 'ALERT']

    #print ("Messages: " + str(len(msg_alert)))
    debug("#apiMessages: {0}".format(len(msg_alert)))
    return msg_alert


def apiEvents() -> Dict:
    # fetch events later than last timestamp
    # return a dictionary

    apiConnect()

    resp_events = requests.get(url=config.getEventUrl(), headers=config.api_token)

    if (resp_events.status_code != 200):
        raise ConnectionError("Could not fetch events. Code: " + str(resp_events.status_code))
    

    #
    # Smappee detects appliances (e.g., washing machine) often with a delay. We cannot rely under filtering for newer timestamps.
    # With a millisecond resolution of the timestamp, we assume that each appears only once. We store seen timestamps instead of the last event and publish only unseen entries.
    # 
 
    #new_events = [x for x in resp_events.json()["events"] if x["timestamp"] > state.last_event]
    
    new_events = [x for x in resp_events.json()["events"] if x["timestamp"] not in state.seen_events]
    
    
    #print ("Events: " + str(len(new_events)))
    debug("#apiEvents: {0}".format(len(new_events)))
    return new_events
    
def apiPowerConsumption() -> Dict:
    # fetch events later than last timestamp
    # return a dictionary

    apiConnect()

    resp_power = requests.get(url=config.getPowerUrl(), headers=config.api_token)

    if (resp_power.status_code != 200):
        raise ConnectionError("Could not fetch power consumption. Code: " + str(resp_power.status_code))
    
    new_power = [x for x in resp_power.json() if x["timestamp"] > state.last_power]
    
    debug("#apiPowerConsumption: {0}".format(len(new_power)))
    return new_power
 
def apiGasConsumption() -> Dict:
    # fetch events later than last timestamp
    # return a dictionary

    apiConnect()

    resp_gas = requests.get(url=config.getGasUrl(), headers=config.api_token)

    if (resp_gas.status_code != 200):
        raise ConnectionError("Could not fetch gas consumption. Code: " + str(resp_gas.status_code))
    
    
    new_gas = [x for x in resp_gas.json()["usages"][0]["intervals"] if x["timestamp"] > state.last_gas and "value" in x ]
    
    debug("#apiGasConsumption: {0}".format(len(new_gas)))
    return new_gas
 
 
def apiWaterConsumption() -> Dict:
    # fetch events later than last timestamp
    # return a dictionary

    apiConnect()

    resp_water = requests.get(url=config.getWaterUrl(), headers=config.api_token)

    if (resp_water.status_code != 200):
        raise ConnectionError("Could not fetch water consumption. Code: " + str(resp_water.status_code))
    
    
    new_water = [x for x in resp_water.json()["usages"][0]["intervals"] if x["timestamp"] > state.last_water and "value" in x ]

    debug("#apiWaterConsumption: {0}".format(len(new_water)))
    return new_water
 

isConnected = False

def eventMqttDisconnect(client, userdata, rc):
    global isConnected
    debug("eventMqttDisconnect")
    isConnected = False

def eventMqttConnect(client, userdata, flags, rc):
    global isConnected
    debug("eventMqttConnect")
    isConnected = True

def mqttConnect() -> bool:
    # reconnect if not connected
    global isConnected
    
    if (not isConnected):
        mqttClient.username_pw_set(config.mqtt_user, config.mqtt_pass)
        mqttClient.on_connect = eventMqttConnect
        mqttClient.on_disconnect = eventMqttDisconnect
        mqttClient.loop_start()
        mqttClient.connect(host=config.mqtt_host, port=config.mqtt_port)
        debug("mqttConnect: login")

    #wait for async connect
    return isConnected

def mqttPubMessages(msg) -> int:
    # connect MQTT
    # publish
    # return last published timestamp or throw and error
    
    last = state.last_message

    if not mqttConnect():
        return last


    # sort by time stamp
    msg = sorted(msg, key=lambda item: item["timestamp"]) 

    for val in msg:
        info = mqttClient.publish(config.getMqttAlertPrefix() + "message", json.dumps(val))
        debug("mqttPubMessages: {0}".format(json.dumps(val)))
        info.wait_for_publish()
        last = val["timestamp"]
    
    # record that this is the last published value. On error, record the last successful timestamp and retry
    
    return last



def mqttPubConsumption(type, msg) -> int:
    # connect MQTT
    # publish
    # return last published timestamp or throw and error


    #print (type + " records: " + str(len(msg)))
    #print (msg)


    if (type.upper() == "GAS"):
        last = state.last_gas
    elif (type.upper() == "WATER"):
        last = state.last_water
    elif (type.upper() == "POWER"):
        last = state.last_power


    if not mqttConnect():
        return last

    # sort by time stamp
    msg = sorted(msg, key=lambda item: item["timestamp"]) 

    for val in msg:
        info = mqtt_res = mqttClient.publish(config.getMqttConsumptionPrefix() + type, json.dumps(val), )
        debug("mqttPubConsumption: {0}-{1}".format(type, json.dumps(val)))
        
        info.wait_for_publish()
        last = val["timestamp"]
    
    # record that this is the last published value. On error, record the last successful timestamp and retry
    return last


def mqttPubEvents(evt):
    # connect MQTT
    # publish 
    # return last published timestamp or throw and error
    
    if not mqttConnect():
        return

    # sort by time stamp
    evt = sorted(evt, key=lambda item: item["timestamp"]) 


    for val in evt:
        info = mqttClient.publish(config.getMqttEventPrefix() + val["source"]["id"] , json.dumps(val))
        debug("mqttPubEvents: {0}-{1}".format(val["source"]["id"], json.dumps(val)))
        info.wait_for_publish()
        # insert in front and trim to max size
        state.seen_events.insert(0, val["timestamp"])
        if len(state.seen_events) > config.eventHistory:
            state.seen_events.pop()



def loop():
    print("Starting monitoring loop")
    global state
    global config

    while True:
        try:
            #print(".")
            # fetch and publish messages
            msg = apiMessages()
            last = mqttPubMessages(msg)
            state.last_message = last

            # fetch and publish events
            evt = apiEvents()
            last = mqttPubEvents(evt)
            
            

            # read power consumption
            msg = apiPowerConsumption()
            last = mqttPubConsumption("power", msg)
            state.last_power = last

            # read gas consumption
            msg = apiGasConsumption()
            last = mqttPubConsumption("gas", msg)
            state.last_gas = last

            # read water consumption
            msg = apiWaterConsumption()
            last = mqttPubConsumption("water", msg)
            state.last_water = last

            # write configuration to file
            writeState()

            # sleep
            time.sleep(config.sleep)
        except ConnectionError as e: 
            # on error: sleep and attempt to re-connect
            logError(e)
            debug("ConnectionError")
            time.sleep(5) # sleep for 5 seconds, then reset the api token
            config.api_token = ""
        except Exception as e:
            logError(e)
            debug("Exception")
            time.sleep(5) # sleep for 5 seconds, then reset the api token


if __name__ == "__main__":
    readSecrets()
    readEnv()
    readState()
    loop()