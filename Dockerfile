FROM python:alpine
LABEL maintainer="rueckix"
LABEL version="0.1"

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY main.py ./
CMD [ "python", "-u", "./main.py" ]